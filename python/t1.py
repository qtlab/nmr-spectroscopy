import matplotlib.pyplot as plt
import numpy as np
from matplotlib import cm
from nmr.data import Series
from nmr import Fit
from nmr import blochz, blochz_multispecies


# Common plot settings
def plt_defaults():
    plt.ylabel('Voltage in V')
    plt.xlabel('Time in ms')
    plt.xlim([0, 53])
    plt.ylim([-5, 5])
    plt.xticks(range(0, 53, 5))
    plt.grid()


# Read CSVs and process again
analysis = Series()
analysis.process(v_err=26.682353168043328)
fits = {
    'l': Fit(analysis, a=0, b=20),
    'lc': Fit(analysis, a=0, b=42, marker='1', color='blue'),
    'full': Fit(analysis, a=0, b=54, marker=',', color='black'),
    'c': Fit(analysis, a=20, b=42),
    'cr': Fit(analysis, a=20, b=54),
    'r': Fit(analysis, a=42, b=54, marker='1', color='purple'),
}

# 1 Plot raw voltage
print('Plot raw voltages')
plt_defaults()
fits['full'].plot_data()
# Fits
fits_raw = [fits['lc'], fits['r']]
for r in fits_raw:
    r.fit()
    r.plot_regression()
    print('χ² =', r.chi_square())
plt.savefig('t1_raw.pdf')
plt.close()
# Residuals
plt_defaults()
plt.ylabel('Voltage residuals in mV')
plt.ylim([-500, 1500])
for r in fits_raw:
    r.plot_residuals()
plt.savefig('t1_raw_residuals.pdf')
plt.close()
print()


def chi_square_3d(start=10, function=blochz):
    chi_square_matrix = np.empty([n, m])
    ndof = analysis.nrows() - start - 2
    for i in range(n):
        for j in range(m):
            analysis.correction(scale=scales[i], offset=offsets[j])
            r = Fit(analysis, function=function, a=start)
            r.fit()
            chi_square_matrix[i][j] = r.chi_square() / ndof
    return chi_square_matrix


# Time consuming least squares analysis
if False:
    # Analyze biases
    print('Analyze biases')
    # offsets = range(-500, 200, 10)
    # scales = np.arange(1.0, 1.2, 0.001)
    chi_squares = []
    chi_voltages = []
    chi_loss_factors = []
    offsets = [-230, -220, -210, -209, -208, -207, -206, -205, -204, -203, -202, -201, -200, -190, -180]
    scales = np.arange(1.150, 1.190, 0.001)
    n = len(scales)
    m = len(offsets)
    times = range(0, 23)
    for start in times:
        chi_square_matrix = chi_square_3d(start=start)
        chi_square_position = np.where(chi_square_matrix == chi_square_matrix.min())
        j = chi_square_position[0][0]
        i = chi_square_position[1][0]
        if chi_square_matrix.min() < 1.74:
            print(start, chi_square_matrix.min(), i, j)
        chi_squares.append(chi_square_matrix.min())
        chi_voltages.append(offsets[i])
        chi_loss_factors.append(round(scales[j], 3))

    print(chi_square_matrix.min())

    # Plot 3d
    plt.close()
    fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
    x, y = np.meshgrid(offsets, scales)
    surf = ax.plot_surface(x, y, chi_square_3d(start=12), cmap=cm.coolwarm, linewidth=0, antialiased=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)
    plt.savefig('3d_plot.pdf')
    plt.close()
    plt.close()

    # Plot chi development
    plt_defaults()
    plt.xlabel('Start time of regression in ms')
    plt.ylabel('$\\chi^2 / \\mathrm{ndf}$')
    plt.xlim([0, 23])
    plt.ylim([0, 50])
    plt.plot(times, chi_squares, 'v')
    plt.savefig('chi_square_dependence.pdf')
    plt.close()
    plt_defaults()
    plt.xlim([0, 23])
    plt.plot(times, chi_voltages, 'v')
    plt.savefig('chi_voltage_dependence.pdf')
    plt.close()
    plt_defaults()
    plt.xlim([0, 23])
    plt.plot(times, chi_loss_factors, 'v')
    plt.savefig('chi_loss_factor_dependence.pdf')
    plt.close()


# 2 Show adjusted results
# Very conclusively found following values:
# Offset -200 mV, Loss factor 1.180
print('Show adjusted results')
plt_defaults()
analysis.correction(scale=1.181, offset=-200)
# Fits
r = Fit(analysis, marker=',', color='black')
r.plot_data()
r.fit()
r.plot_regression()
print('χ² =', r.chi_square())
print('T₁ =', r.t1)
r2 = Fit(analysis, a=10, marker=',', color='black')
r2.fit()
r2.plot_regression()
print('χ² =', r2.chi_square())
print('T₁ =', r2.t1)
plt.savefig('t1_adjusted.pdf')
plt.close()

resmax = 300
# Residuals
plt_defaults()
plt.ylabel('Voltage residuals in mV')
plt.ylim([-500, 300])
r.plot_residuals()
plt.savefig('t1_adjusted_residuals.pdf')
plt.close()
# Residuals cut off
plt_defaults()
plt.ylabel('Voltage residuals in mV')
plt.ylim([-resmax, resmax])
r2.plot_residuals()
plt.savefig('t1_adjusted_residuals_cutoff.pdf')
plt.close()
print()

# 3 Super fit
print('Multi-species regression')
plt_defaults()
r = Fit(analysis, function=blochz_multispecies, marker=',', color='black')
r.plot_data()
r.fit()
ndf = analysis.nrows() - 4
print('χ² =', r.chi_square())
print('ndf =', ndf)
print('χ² / ndf =', r.chi_square() / ndf)
print('T₁ =', r.t1, 'M_eq =', r.m0)
print('T₁\' =', r.t1_2, 'M_eq\' =', r.m0_2)
print('χ² =', r.chi_square())
r.plot_regression()
plt.savefig('t1_multispecies.pdf')
plt.close()
print()

# Residuals
plt_defaults()
plt.ylabel('Voltage residuals in mV')
plt.ylim([-resmax, resmax])
r.plot_residuals()
plt.savefig('t1_multispecies_residuals.pdf')
print()


# 4 Analyze biases
print('Multi-species bias analysis')
# offsets = range(-500, 200, 10)
# scales = np.arange(1.0, 1.2, 0.001)
offsets = [-230, -220, -210, -209, -208, -207, -206, -205, -204, -203, -202, -201, -200, -190, -180]
scales = np.arange(1.170, 1.190, 0.001)
n = len(scales)
m = len(offsets)
times = range(0, 23)
chi_square_matrix = chi_square_3d(function=blochz_multispecies)

# # Plot 3d
# plt.close()
# fig, ax = plt.subplots(subplot_kw={"projection": "3d"})
# x, y = np.meshgrid(offsets, scales)
# surf = ax.plot_surface(
#     x,
#     y,
#     chi_square_3d(function=blochz_multispecies),
#     cmap=cm.coolwarm,
#     linewidth=0,
#     antialiased=False
# )
# fig.colorbar(surf, shrink=0.5, aspect=5)
# plt.savefig('3d_plot_2.pdf')
# plt.close()

chi_square_position = np.where(chi_square_matrix == chi_square_matrix.min())
i = chi_square_position[1][0]
j = chi_square_position[0][0]
print('χ² / ndf =', chi_square_matrix.min())
print('At ΔU =', offsets[i], 'and loss factor =', round(scales[j], 3))
print()

# 5 FINAL Results
print('Show FINAL results')
plt.close()
plt_defaults()
analysis.correction(scale=1.181, offset=-207)
# Fits
r = Fit(analysis, function=blochz_multispecies, marker=',', color='black')
r.plot_data()
r.fit()
r.plot_regression()
ndf = analysis.nrows() - 4
print('χ² =', r.chi_square())
print('ndf =', ndf)
print('χ² / ndf =', r.chi_square() / ndf)
print('T₁ =', r.t1)
print('T₁\' =', r.t1_2)
plt.savefig('t1_final.pdf')
plt.close()

# Residuals
plt_defaults()
plt.ylabel('Voltage residuals in mV')
plt.ylim([-resmax, resmax])
r.plot_residuals()
plt.savefig('t1_final_residuals.pdf')
print()

# Constituents
plt.close()
plt_defaults()
plt.ylim([-1, 1])
t = np.linspace(0, 53, 2**13)
y1 = blochz(t, r.t1, r.m0) / r.m0
y2 = r.m0_2 * np.exp(-t/r.t1_2)/r.m0
plt.axhline(0, linewidth='1', color='k')
plt.plot(t, y1, '--', color='green', label='Ample supply of protons')
plt.plot(t, y2, '--', color='red', label='Off-resonance nuclei')
plt.plot(t, y1+y2, color='k', label='Fit result')
plt.ylabel('Magnetization in units of $M_{\\mathrm{eq}}$')
plt.legend()
plt.savefig('multispecies.pdf')
print()

# Results with simulation
print('Include simulation in results')
plt.close()
plt_defaults()
analysis.correction(scale=1.181, offset=-207)
# Fits
r = Fit(analysis, function=blochz_multispecies, marker=',', color='red', label='Measurement')
r.fit()

# Theory
t = np.linspace(0, 54, 2**13)
v_theory = blochz(t, r.t1, 1.0)
plt.plot(t, v_theory, linewidth=0.65, color='black', label='Theory')
v_theory_2 = blochz(t, r.t1, r.m0) / r.m0 + r.m0_2 * np.exp(-t/r.t1_2)/r.m0
plt.plot(t, v_theory_2, linewidth=0.65, color='black')

# Simulation
taus = np.loadtxt('data/sim/t1_taus.txt')
v_simulation = np.loadtxt('data/sim/t1_voltages.txt')
for i in range(20):
    v_simulation[i] *= -1
plt.plot(taus, v_simulation, '.', color='orange', label='Simulation')
# Plot data
r.plot_normalized()
#r.plot_regression()
plt.xlim([0, 54])
plt.ylim([-1, 1])
plt.ylabel('Magnetization in units of $M_\\mathrm{eq}$')
plt.legend()
plt.savefig('t1_with_simulation.pdf')
plt.close()

# Residuals
plt.close()
plt_defaults()
plt.ylim([-0.05, 0.05])

v_simulation_theory = blochz(taus, r.t1, 1.0)
plt.ylabel('Relative magnetization residuals')
plt.axhline(0, linewidth=1, color='black', label='_nolegend_')
r.plot_residuals(normalize=True)
plt.plot(
    taus,
    (v_simulation - v_simulation_theory) / v_simulation_theory,
    linewidth=0,
    marker='.',
    color='orange',
)
plt.legend(['Simulation residuals', 'Adjusted measurement residuals'])
plt.savefig('t1_with_simulation_residuals.pdf')
plt.close()


df = analysis.output()
df.to_csv('t1.csv', index=False)
