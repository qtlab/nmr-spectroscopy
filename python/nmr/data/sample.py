import numpy as np
import pandas as pd
from scipy.signal import find_peaks
from nmr.constants import *


class Sample:
    """
    Reads oscilloscope csv data into Python

    Represents a single oscilloscope snapshot and not the entire
    collection of data.

    Has a library of analysis-agnostic functions that are needed
    to process the data.
    """
    def __init__(self, data, experiment='T1'):
        self.experiment = experiment
        # Fill list of files to be read
        self.csv_file = data
        self.data = pd.read_csv(self.csv_file, usecols=[3, 4], names=["t", "V"])  # in s, V
        if self.experiment[0] == 'T':
            self.peaks = self.fid_peaks()  # in s, V
            self.dips = self.dips()  # in s
        else:
            self.tau_step = int(TAU_PULSE_STEP[self.csv_file] / 1000 / TAU_RESOLUTION)
            self.peaks = self.peak_sequences()
            self.voltages = self.sequence_voltages()
            self.indices = self.sequence_taus()
            self.voltage_errors = np.zeros(len(self.voltages), dtype=np.float128)
        # Noise from all measurements
        self.noise_voltages = self.noise_calibration()

    def fid_peaks(self):  # in s and V
        """
        Finds FID peaks

        Quickly finds peaks > 600 mV and keeps searching for peaks between
        400 and 600 mV.

        :return: list of (t, V) in seconds and volts
        """
        result = []
        # Returns indices and peak heights
        indices = [None]
        relax_height_requirement = 0

        while len(indices) < PEAK_COUNT[self.experiment]:
            v_min = MIN_PEAK_HEIGHT['default'] - relax_height_requirement
            indices, _ = find_peaks(
                self.data['V'],
                height=v_min/1000,
                distance=MIN_PEAK_DISTANCE,
            )
            relax_height_requirement += 10
        for index in indices:
            peak = self.data['t'][index], self.data['V'][index]
            result.append(peak)
        return result

    def peak_sequences(self):
        result = []
        indices, _ = find_peaks(
            self.data['V'],
            height=MIN_PEAK_HEIGHT[self.csv_file],
            distance=round(self.tau_step * TAU_PULSE_PEAK_DISTANCE_FACTOR),
        )
        for index in indices:
            peak = self.data['t'][index], self.data['V'][index]
            result.append(peak)
        return result

    def dips(self):  # in s
        """
        Finds dips

        Looks for dips from ~ 320 mV to roughly 80 mV.

        Returns list of times in seconds
        """
        result = []
        # Returns indices and dip depths
        indices = []
        relax_height_requirement = 0
        while len(indices) < PEAK_COUNT[self.experiment]:
            v_min = -0 - relax_height_requirement
            indices, _ = find_peaks(
                -1*self.data['V'],  # flip to negative voltages
                height=v_min/1000,
                distance=MIN_PEAK_DISTANCE,
            )
            relax_height_requirement += 1
        for index in indices:
            dip = self.data['t'][index]
            result.append(dip)
        return result

    def fid_delay(self):  # in µs
        """
        Measures time delay of FID peak

        Probably only needed to estimate bias
        """
        # second peak pos - first peak pos
        if self.experiment == 'T1':
            return int(1E6*(self.peaks[1][0] - self.peaks[0][0]))
        elif self.experiment == 'T2':
            return int((self.peaks[2][0] - self.peaks[0][0]))
        else:
            return 0

    def dip_delay(self):  # in µs
        """
        Measures time delay in ms of FID's dip (prior to peak)
        """
        if self.experiment == 'T1':
            return int(1E6*(self.dips[1] - self.dips[0]))
        elif self.experiment == 'T2':
            return int(1E6*(self.dips[1] - self.dips[0]))
        else:
            return 0

    def tau(self):  # in µs
        """
        Returns delay time tau in ms.

        Chooses peak or dip delay to estimate tau.

        Note that tau (ms) is integer. The effects of tau inaccuracy will be analyzed separately.
        """
        t1 = self.fid_delay()
        t2 = self.dip_delay()
        # If both dip and peak detection yield same result
        if abs(t1 - t2) > 100 and self.fid_voltage() > 100:  # us and mV
            return 0
        return t2

    def fid_voltage(self):
        """
        Returns FID peak voltage in units of mV

        This is the purpose of the pre-processing analysis

        The dataset contains voltages in units of 4 x 10 mV.
        """
        # second peak height - offset
        if self.experiment == 'T1':
            return int(1000 * self.peaks[1][1])
        elif self.experiment == 'T2':
            return int(1000 * self.peaks[-1][1])
        else:
            return 0

    def sequence_voltages(self):
        voltages = []
        for i in range(1, len(self.peaks)):
            if i % 2 == 0:
                voltages.append(int(1000 * self.peaks[i][1]))
        return np.asarray(voltages)

    def sequence_taus(self):  # in ms
        taus = []
        for i in range(len(self.voltages)):
            # Every second pulse
            taus.append(TAU_PULSE_STEP[self.csv_file] * 2 * (i + 1))
        return np.asarray(taus)

    def noise_calibration(self):
        """
        Filters peaks yielding only noise data

        :return: Array with noise
        """
        # Just use the few milliseconds before the first peak
        self.noise_voltages = self.data.copy('tV')
        self.noise_voltages = self.noise_voltages[self.noise_voltages['t'] < -1E-3]
        return self.noise_voltages['V']

    def nrows(self):
        return len(self.voltages)
