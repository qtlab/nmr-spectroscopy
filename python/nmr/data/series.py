import numpy as np
import pandas as pd
from nmr.data.sample import Sample
from nmr.constants import *


class Series:
    """
    Performs the actual analysis

    Specific for experiment and structure of data
    """
    def __init__(self, experiment='T1'):
        self.experiment = experiment
        # Fill list of files to be read
        self.csv_files = []
        self.datasets = []
        self.input()
        # Data arrays
        self.indices = np.empty(self.nrows(), dtype=np.uint32)  # 1..53
        self.taus = np.empty(self.nrows(), dtype=np.uint32)  # 1..53 ms
        self.voltages = np.zeros(self.nrows(), dtype=np.int32)  # -8000 mV .. 8000 mV
        # Analysis arrays
        self.voltage_errors = np.empty(self.nrows(), dtype=np.float64)  # in mV
        self.peak_delays = np.empty(self.nrows(), dtype=np.float64)  # in ms
        self.dip_delays = np.empty(self.nrows(), dtype=np.float64)  # in ms
        self.voltages_raw = None
        self.noise_voltages = None

    def input(self):
        """
        Reads CSVs of all associated measurements
        """
        for i in FILE_RANGES[self.experiment]:
            if self.experiment == 'T1':
                # Wrong trigger at tau = 41 ms
                if i == 48:
                    continue
                # Duplicate value with visually indistinguishable FID
                elif i == 15:
                    continue
            self.csv_files.append(f'data/{self.experiment}/ALL{i:04}/F{i:04}CH1.CSV')

    def process(self, v_err=30.0):
        # This is independent and everything is thrown onto same pile
        noise = []  # list of arrays
        for i in range(len(self.csv_files)):
            # Input
            ds = Sample(self.csv_files[i], self.experiment)
            self.datasets.append(ds)

            # T1 and T2
            if self.experiment in ['T1', 'T2']:
                self.indices[i] = i + 1
                self.voltage_errors[i] = v_err
                self.voltages[i] = ds.fid_voltage()
                if self.experiment == 'T1':
                    # Adjust voltages for low voltage signals
                    if self.indices[i] < TAU_ZERO_CROSSING - 1:
                        self.voltages[i] *= -1  # flip
                    elif self.indices[i] in [TAU_ZERO_CROSSING - 1, TAU_ZERO_CROSSING, TAU_ZERO_CROSSING + 1]:
                        self.voltages[i] = 0  # no signal

                self.dip_delays[i] = ds.dip_delay()
                self.taus[i] = ds.tau()  # in ms

            elif self.experiment in ['CP', 'MG']:
                ds.voltage_errors[:] = v_err

            noise.append(ds.noise_voltages)
        self.voltages_raw = self.voltages.copy()

        # Noise
        self.noise_voltages = np.asarray(noise, dtype=object)
        # Throw all noise from all csvs into one big array
        self.noise_voltages = np.concatenate(self.noise_voltages).ravel()

        # No signal means the signal could be anywhere
        # between -380 mV and 380 mV with equal probability
        if self.experiment == 'T1':
            voltage_noise_sigma = 2*380/np.sqrt(12)  # mV
            self.voltage_errors[19:22] = voltage_noise_sigma

    def output(self):
        df = pd.DataFrame(index=self.indices)
        df['tau'] = self.taus
        df['V'] = self.voltages
        return df

    def nrows(self):
        if self.experiment in ['T1', 'T2']:
            return len(self.csv_files)
        else:
            return PEAK_COUNT[self.experiment]//2

    def correction(self, scale=1.0, offset=0):
        self.voltages = self.voltages_raw.copy()
        # First stretch
        for i in range(self.nrows()):
            if self.indices[i] > TAU_DISCONTINUITY:
                # It does not matter that we are rounding to mV.
                # Digit error ~ 10 mV, Fluctuations ~ 30 mV
                self.voltages[i] *= scale
        # Then offset
        for i in range(self.nrows()):
            if self.indices[i] < TAU_ZERO_CROSSING - 1:
                self.voltages[i] -= offset
            elif self.indices[i] > TAU_ZERO_CROSSING + 1:
                self.voltages[i] += offset