import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from nmr.constants import *


#@staticmethod
def blochz(t, t1, m0):
    return m0 * (1.0 - 2 * np.exp(-np.float64(t) / t1))

#@staticmethod
def blochz_multispecies(t, t1, m0, t1_2, m0_2):
    return blochz(t, t1, m0) + m0_2 * np.exp(-np.float64(t) / t1_2)

#@staticmethod
def blochxy(t, t2, m0, u0):
    return m0 * np.exp(-np.float64(t) / t2) + u0


class Fit:

    def __init__(self, data, function=blochz, a=0, b=-1, marker='X', color=None, label=None, unit=1000):
        self.data = data
        self.function = function
        self.a = np.int8(a)
        self.b = np.int8(b)
        if b == -1:
            self.b = data.nrows()
        self.indices = None  # 1..53
        self.indices_uncut = None
        self.voltages = None  # -8000 mV .. 8000 mV
        self.voltages_uncut = None
        self.voltage_errors = None  # in mV
        self.voltage_errors_uncut = None
        self.t1 = np.float128
        self.m0 = np.float128
        self.t1_2 = np.float128
        self.m0_2 = np.float128
        self.u0 = np.float128
        # Plot options
        self.marker = marker
        self.color = color
        self.label = label
        self.voltage_unit = unit  # in mV

        # Read out data from oscilloscope.Dataset type object
        if data:
            self.read(data)

    def read(self, data):
        interval = slice(self.a, self.b)
        self.indices = data.indices[interval]
        self.indices_uncut = data.indices.copy()
        self.voltages = data.voltages[interval]
        self.voltages_uncut = data.voltages.copy()
        self.voltage_errors = data.voltage_errors[interval]
        self.voltage_errors_uncut = data.voltage_errors.copy()

    def fit(self):
        if self.function == blochz_multispecies:
            bounds = ([20, 3500.0, 2, 1500.0], [35, 10000.0, 10, 4500.0])
        elif self.function == blochxy:
            bounds = ([1, 100.0, 0.0], [50, 10000.0, 200.0])
        else:
            bounds = ([20, 3000.0], [35, 10000.0])
        popt, pcov = curve_fit(self.function, self.indices, self.voltages, sigma=self.voltage_errors, bounds=bounds)
        self.t1 = popt[0]
        self.m0 = popt[1]
        if self.function == blochz_multispecies:
            self.t1_2 = popt[2]
            self.m0_2 = popt[3]
        if self.function == blochxy:
            self.u0 = popt[2]
        return popt

    def chi_square(self):
        if self.function == blochz_multispecies:
            theory_voltages = self.function(self.indices, self.t1, self.m0, self.t1_2, self.m0_2)
        elif self.function == blochxy:
            theory_voltages = self.function(self.indices, self.t1, self.m0, self.u0)
        else:
            theory_voltages = self.function(self.indices, self.t1, self.m0)
        return np.sum(((self.voltages - theory_voltages) / self.voltage_errors) ** 2)

    def ndf(self):
        return len(self.indices) - 3

    def plot_data(self):
        color = self.color
        if color == 'black':
            color = 'red'
        plt.errorbar(
            self.indices_uncut,
            self.voltages_uncut / self.voltage_unit,
            yerr=self.voltage_errors_uncut / self.voltage_unit,
            linewidth=0,
            elinewidth=1,
            capsize=2,
            marker=self.marker,
            color=color,
            label=self.label,
        )

    def plot_regression(self):
        # t = np.linspace(self.indices.min(), self.indices.max(), PLOT_RESOLUTION)
        t = np.linspace(-10, 100, 3*PLOT_RESOLUTION)
        if self.function == blochz_multispecies:
            y = self.function(t, self.t1, self.m0, self.t1_2, self.m0_2) / self.voltage_unit
        elif self.function == blochxy:
            y = self.function(t, self.t1, self.m0, self.u0) / self.voltage_unit
        else:
            y = self.function(t, self.t1, self.m0) / self.voltage_unit
        plt.plot(
            t,
            y,
            '--',
            linewidth=1,
            color=self.color
        )

    def normalize(self, y):
        yy = y.astype(np.float128)
        if self.function == blochz_multispecies:
            return yy/self.m0
        elif self.function == blochxy:
            return (yy - self.u0)/self.m0
        else:
            return yy/self.m0

    def plot_normalized(self):
        plt.errorbar(
            self.indices_uncut,
            self.normalize(self.voltages_uncut),
            yerr=self.voltage_errors_uncut/self.m0,
            linewidth=0,
            elinewidth=1,
            capsize=2,
            marker=self.marker,
            color=self.color,
            label=self.label,
        )

    def plot_residuals(self, normalize=False):
        if self.function == blochz_multispecies:
            voltages_theory = self.function(self.indices, self.t1, self.m0, self.t1_2, self.m0_2)
        else:
            voltages_theory = self.function(self.indices, self.t1, self.m0)
        y = self.voltages - voltages_theory
        yerr = self.voltage_errors
        if normalize:
            y /= voltages_theory
            yerr /= voltages_theory
        plt.errorbar(
            self.indices,
            y,
            yerr=yerr,
            linewidth=0,
            elinewidth=1,
            capsize=2,
            marker='d',
            color=self.color,
            label=self.label,
        )
