import numpy as np
from qopt import *
from scipy.signal import find_peaks
from .constants import *
import time


TIME_UNIT = 100E-6  # s
STEP_DURATION = 1  # TIME_UNIT
N_TRACES = 30


class Simulation:

    def __init__(self,
                 duration,
                 tau,
                 experiment='T1',
                 gamma=1,
                 delta_omega=None,
                 ):
        """
        Simulates PNMR experiment using qopt

        duration:       Total duration of one simulated measurement in ms
        tau:            Waiting time
        experiment:     T1 or T2
        gamma:          Strength of the Markovian noise
        delta_omega:    Standard deviation of the quasi static noise
        """

        self.duration = duration
        self.tau = tau
        self.experiment = experiment

        # Simulation data
        self.indices = []  # np.linspace(0, self.duration, self.nsteps()+1)  # time
        self.voltages = []  # xy magnetization
        self.m = []  # z magnetization
        self.ensemble_average = None  # spin xy-projections for T2 measurement
        self.peaks = None

        # Parameters
        self.gamma = gamma
        if delta_omega is None:
            self.delta_omega = 1.0 / Simulation._time_from_si(0.0002) / 2.8 / 0.7
        self.n_traces = N_TRACES

        # Initialization
        self.solver = self._solver()
        self.time_evolutions = None
        if self.experiment == 'T1':
            self.ctrl_parameters = self._t1_parameters()
        elif self.experiment == 'T2':
            self.ctrl_parameters = self._t2_parameters()

        # Bookkeeping
        self.runtime = time.perf_counter()  # Replace with runtime after sim

    def process(self):
        """Perform simulation"""
        self.solver.set_optimization_parameters(self.ctrl_parameters)
        self.time_evolutions = self.solver.forward_propagators
        for time_evolution in self.time_evolutions:

            # Measure z magnetization
            rho_z = DenseOperator(np.reshape(time_evolution.data, (2, 2)).T)
            self.m.append(np.real((DenseOperator.pauli_z() * rho_z).tr()))

            # Measure xy magnetization
            self.voltages.append(Simulation.norm_xy_spin_projection(time_evolution))

        # Measure ensemble average for T2
        self.ensemble_average = self.ensemble_averaged_spin_projection()

        # Peak finder
        self.peaks = self.peak_sequences()
        self.indices, self.voltages = self.sequence_peak_data()

        # Bookkeeping
        self.runtime = time.perf_counter() - self.runtime

    def nsteps(self, t=None) -> int:
        """Number of time segments"""
        if t is None:
            t = Simulation._time_from_si(self.duration / 1000.0)
        return int(t)

    def _t1_parameters(self):
        """T1 pulse sequence"""
        parameters = np.zeros((self.nsteps(), 2))
        # Do not model the actual pulse. We can assume the pulse as instantaneous
        t_pulse = Simulation._time_from_si(STEP_DURATION * TIME_UNIT)
        n_pulse = self.nsteps(t_pulse)
        t_relax = Simulation._time_from_si(self.tau / 1000.0)
        n_relax = self.nsteps(t_relax)
        # Do pi pulse
        parameters[:n_pulse, 0] = np.pi / t_pulse
        # Wait self.tau ms
        # Then do pi/2 pulse
        parameters[n_relax + n_pulse:n_relax + 2*n_pulse, 0] = np.pi / 2.0 / t_pulse
        return parameters

    def _t2_parameters(self):
        """T2 pulse sequence"""
        parameters = np.zeros((self.nsteps(), 2))
        # Do not model the actual pulse. We can assume the pulse as instantaneous
        t_pulse = Simulation._time_from_si(STEP_DURATION * TIME_UNIT)
        n_pulse = self.nsteps(t_pulse)
        t_relax = Simulation._time_from_si(self.tau / 1000.0)
        n_relax = self.nsteps(t_relax)
        # Do pi/2 pulse
        parameters[:n_pulse, 0] = np.pi / 2.0 / t_pulse
        # Wait self.tau ms
        # Then do pi pulse
        for i in range(13):
            parameters[(2*i + 1) * n_relax:(2*i + 1) * n_relax + n_pulse, 0] = np.pi / t_pulse
        return parameters

    def t1_measurement(self):
        return np.max(self.voltages)

    def t2_measurement(self):
        return np.max(self.voltages[self.tau * 2 * 2 - 5:])

    def peak_sequences(self):
        result = []
        indices, _ = find_peaks(
            self.ensemble_average,
            height=0.05,
            distance=round(2 * self.tau / 1000 / TIME_UNIT * 0.75),
        )
        # print(indices)
        for i in range(len(indices)):
            time = indices[i]
            # print(time * TIME_UNIT)
            peak = time * TIME_UNIT, self.ensemble_average[time]
            result.append(peak)
        return result

    def sequence_peak_data(self):
        voltages = []
        taus = []
        for i in range(len(self.peaks)):
            voltages.append(self.peaks[i][1])
            taus.append((1000 * self.peaks[i][0]))
        return np.asarray(taus), np.asarray(voltages)

    def ensemble_averaged_spin_projection(self):
        """
        The spin echo requires that the Monte Carlo simulation is understood as
        ensemble average. Thus we need to sum over the nuclear spins, before we
        take the norm.
        """
        _n_time_steps = self.solver.transferred_time.size
        _n_traces = self.solver.noise_trace_generator.n_traces
        spin_projections = np.zeros((_n_time_steps + 1, _n_traces, 2))
        for trace in range(self.n_traces):
            for t in range(self.nsteps() + 1):
                spin_projections[t, trace, :] = Simulation.xy_spin_projection(
                    self.solver.forward_propagators_noise[trace][t]
                )
        spin_projections = np.linalg.norm(
            np.mean(spin_projections, axis=1),
            axis=1
        )
        return spin_projections

    @staticmethod
    def xy_spin_projection(time_evolution):
        rho_xy = DenseOperator(np.reshape(time_evolution.data, (2, 2)).T)
        return np.real([
            (DenseOperator.pauli_x() * rho_xy).tr(),
            (DenseOperator.pauli_y() * rho_xy).tr()
        ])

    @staticmethod
    def norm_xy_spin_projection(time_evolution):
        _xy = Simulation.xy_spin_projection(time_evolution)
        return np.linalg.norm(_xy)

    @staticmethod
    def _time_from_si(time) -> int:
        """Converts SI time to used time units"""
        return int(time / TIME_UNIT * STEP_DURATION)

    @staticmethod
    def _hamiltonian_to_super_operator(h):
        """Creates the Lindblad master equation"""
        sup_op = h.identity_like().kron(h) - h.transpose().kron(h.identity_like())
        return sup_op

    def _solver(self):
        """
        Solver for the NMR atom which includes Markovian and quasi static noise

        The simulation starts initially in the down state.
        """
        time_steps = STEP_DURATION * np.ones(self.nsteps())

        lindblad_op = DenseOperator.pauli_m()
        dissipative_op = lindblad_op.conjugate().kron(lindblad_op) - .5 * (
                lindblad_op.identity_like().kron(
                    lindblad_op.dag() * lindblad_op
                )
                + (lindblad_op.transpose() * lindblad_op.conj()).kron(lindblad_op.identity_like())
        )

        h_drift = 1j * self.gamma * dissipative_op
        # The factor of 1j cancels the -1j by which the operator is multiplied,
        # because it is treated as Hamiltonian.

        h_ctrl = [
            self._hamiltonian_to_super_operator(.5 * DenseOperator.pauli_x()),
            self._hamiltonian_to_super_operator(-.5 * DenseOperator.pauli_z())
        ]

        ntg_spin_spin_relaxation = NTGQuasiStatic(
            standard_deviation=[self.delta_omega],
            n_samples_per_trace=self.nsteps(),
            n_traces=self.n_traces
        )

        down_state = DenseOperator(np.asarray([[0], [0], [0], [1]]))

        solver = SchroedingerSMonteCarlo(
            h_drift=h_drift,
            h_ctrl=h_ctrl,
            tau=time_steps,
            noise_trace_generator=ntg_spin_spin_relaxation,
            h_noise=[self._hamiltonian_to_super_operator(.5 * DenseOperator.pauli_z())],
            initial_state=down_state
        )

        return solver
