PLOT_RESOLUTION = int(2**13)

MIN_PEAK_DISTANCE = 50  # µs
PEAK_SAFETY_MARGIN = 5E-3  # 3 ms

TAU_MIN = 1
TAU_MAX = {
    'T1': 53,
    'T2': 37
}
TAU_ZERO_CROSSING = 21
TAU_DISCONTINUITY = 42.5
TAU_RESOLUTION = 40E-6

FILE_RANGES = {
    'T1': range(7, 62),
    'T2': range(62, 99),
    'CP': [22, 24],
    'MG': [23, 25],
}
PEAK_COUNT = {
    'T1': 2,
    'T2': 3,
    'CP': 2*11,
    'MG': 2*14
}

MIN_PEAK_HEIGHT = {
    'default': 1200,  # mV
    # CP
    'data/CP/ALL0022/F0022CH1.CSV': 0.161,  # V
    'data/CP/ALL0024/F0024CH1.CSV': 0.175,  # V
    # MG
    'data/MG/ALL0023/F0023CH1.CSV': 0.161,  # V
    'data/MG/ALL0025/F0025CH1.CSV': 0.161,  # V
}
TAU_PULSE_STEP = {
    'data/CP/ALL0022/F0022CH1.CSV': 4,  # ms
    'data/CP/ALL0024/F0024CH1.CSV': 3,  # ms
    'data/MG/ALL0023/F0023CH1.CSV': 4,  # ms
    'data/MG/ALL0025/F0025CH1.CSV': 3,  # ms
}
TAU_PULSE_PEAK_DISTANCE_FACTOR = 0.75
TAU_DISTANCE_FACTOR = {
    'data/CP/ALL0022/F0022CH1.CSV': 0.75,  # s
    'data/CP/ALL0024/F0024CH1.CSV': 0.75,  # s
    'data/MG/ALL0023/F0023CH1.CSV': 0.75,  # s
    'data/MG/ALL0025/F0025CH1.CSV': 0.75,  # s
}