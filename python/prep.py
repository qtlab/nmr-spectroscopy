import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from nmr.data import Series


def fit_norm(v, a, b, scale=1.0):
    n = len(v)
    mu, sigma = norm.fit(v)
    x = np.linspace(a, b, 10000)
    y = scale * n * norm.pdf(x, mu, sigma)
    plt.plot(x, y)
    return mu, sigma, n


def chi_square(y1, y2, yerr):
    return np.sum(((y1 - y2)/yerr)**2)


# Read CSVs. Inefficient
t1 = Series()
t1.process()
t2 = Series(experiment='T2')
t2.process()
cp = Series(experiment='CP')
cp.process()
mg = Series(experiment='MG')
mg.process()
analyses = []
analyses.append(t1)
analyses.append(t2)
analyses.append(cp)
analyses.append(mg)

# Time calibration
residuals = []
for i in range(len(t1.taus)):
    if t1.taus[i] != 0:
        residuals.append(t1.indices[i]*1000 - t1.taus[i])
bins = [-15, -5, 5, 15, 25, 35]
plt.hist(residuals, bins=bins)
plt.xlabel('Nominal delay $\\tau - $ blanking delay $\\tau_\\mathrm{dip}$ in $\\mu$s')
plt.ylabel('Count')
plt.savefig('calibration_time.pdf')
plt.close()

def calibration(ana):
    # Voltage calibration
    ticks = np.arange(xlim[0], 500, resolution)
    plt.xticks(ticks)
    plt.xlim(xlim)
    plt.hist(ana.noise_voltages*1000, bins)
    fit_result = fit_norm(ana.noise_voltages*1000, xlim[0], xlim[1], scale=resolution)
    plt.xlabel('Voltage in V')
    plt.ylabel('Count')
    plt.ticklabel_format(axis="y", style="sci", scilimits=(0, 0))
    plt.savefig(ana.experiment + '_calibration_voltage.pdf')
    plt.close()
    print('\n' + ana.experiment + ' voltage calibration')
    print(f'µ = {fit_result[0]} ± {fit_result[0]/np.sqrt(fit_result[2])}')
    print(f'σ = {fit_result[1]}')
    print()

resolution = 40
xlim = [200, 400]
bins = np.arange(0 - resolution / 2, 500 + resolution / 2, resolution)
calibration(t1)
calibration(t2)
resolution = 4
xlim = [144, 164]
bins = np.arange(0 - resolution / 2, 500 + resolution / 2, resolution)
calibration(cp)
calibration(mg)
