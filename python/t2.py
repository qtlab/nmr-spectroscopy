import numpy as np
import matplotlib.pyplot as plt
from nmr.data import Series
from nmr import Fit
from nmr import blochxy


# Common plot settings
def plt_defaults():
    plt.ylabel('Voltage in mV')
    plt.xlabel('Time in ms')
    plt.xticks(range(0, 81, 5))
    plt.xlim([-2, 83])
    plt.xlim()
    plt.grid()
    plt.legend(prop={'family': 'monospace'})


# Read CSVs and process again
manual = Series(experiment='T2')
manual.process(v_err=26.32535802769066)
cp = Series(experiment='CP')
mg = Series(experiment='MG')
cp.process(v_err=2.196724834839357)
mg.process(v_err=2.1043763921884313)

fits = []
regression_manual = Fit(manual, function=blochxy, a=8, marker=',', color='black', label='$\\mathrm{Single\\ pulse}$')
fits.append(Fit(cp.datasets[0], function=blochxy, marker='X', color='red', label='CP $\\tau=4\\,\\mathrm{ms}$'))
fits.append(Fit(cp.datasets[1], function=blochxy, marker='X', color='darkorange', label='CP $\\tau=3\\,\\mathrm{ms}$'))
fits.append(Fit(mg.datasets[0], function=blochxy, marker='X', color='blue', label='MG $\\tau=4\\,\\mathrm{ms}$'))
fits.append(Fit(mg.datasets[1], function=blochxy, marker='X', color='purple', label='MG $\\tau=3\\,\\mathrm{ms}$'))

print('Plot T2 measurement')
plt.close()
regression_manual.plot_data()
regression_manual.fit()
regression_manual.plot_regression()
print('χ² =', regression_manual.chi_square() / regression_manual.ndf(), 'ndf')
print('T₁ =', regression_manual.t1)
plt_defaults()
plt.ylim([0, 4.8])
plt.xlim([-2, 40])
plt.ylabel('Voltage in V')
plt.savefig('t2_raw.pdf')
plt.close()
print()

print('Plot voltage')
for r in fits:
    r.plot_data()
    r.fit()
    r.plot_regression()
    print('χ² =', r.chi_square() / r.ndf(), 'ndf')
    print('T₁ =', r.t1, 'U_0 =', r.u0)
plt_defaults()
plt.ylim([0, 1])
plt.savefig('t2_sequence.pdf')
plt.close()
print()

print('Plot normalized')
regression_manual.marker = '.'
_, ax = plt.subplots()
ax.axhline(y=0, color='black', linewidth=1)
for r in [regression_manual, fits[0], fits[1]]:
    r.plot_normalized()
    r.fit()
    print('χ² =', r.chi_square() / r.ndf(), 'ndf')
    print('T₁ =', r.t1, 'U_0 =', r.u0)

# Simulation
taus = np.loadtxt('data/sim/t2_taus.txt')
v_simulation = np.loadtxt('data/sim/t2_voltages.txt')
plt.plot(taus, v_simulation, '.', color='orange', label='Simulation')
plt_defaults()
plt.ylabel('Magnetization in units of $M_{\\mathrm{eq}}$')
plt.savefig('t2_sequence_normalized.pdf')
print()


df = manual.output()
df.to_csv('t2.csv', index=False)
