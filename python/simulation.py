import numpy as np
import matplotlib.pyplot as plt
from nmr import Simulation


SIMULATIONS = {
    # 'T1',
    'T2',
}


# Simulate T1 experiment
if 'T1' in SIMULATIONS:
    print('Simulate T1 measurement')
    gamma = 1 / Simulation._time_from_si(time=0.02996680740565491)
    sims = []
    taus = []
    voltages = []
    for i in range(1, 54):
        print(f'Simulation {i} of 53')
        sim = Simulation(54, i, experiment='T1', gamma=gamma)
        sim.process()
        sims.append(sim)
        taus.append(sim.tau)
        voltages.append(sim.t1_measurement())

    taus = np.asarray(taus)
    voltages = np.asarray(voltages)
    np.savetxt('data/sim/t1_taus.txt', taus)
    np.savetxt('data/sim/t1_voltages.txt', voltages)

    print(sim.runtime)

# Simulate T2 experiment
if 'T2' in SIMULATIONS:
    print('Simulate T2 measurement')
    gamma = 1 / Simulation._time_from_si(time=0.014628934261189618)
    sims = []
    taus = []
    voltages = []
    for i in [2, 3, 5, 7, 11, 13, 17, 19]:  # range(1, 41):
        print('Simulation T2')
        sim = Simulation(80, i, experiment='T2', gamma=gamma)
        sim.process()
        # print(sim.peak_sequences())
        sims.append(sim)
        for j in range(len(sim.indices)):
            taus.append(sim.indices[j])
            voltages.append(sim.voltages[j])
        plt.plot(sim.ensemble_average)

    plt.show()
    plt.close()
    plt.plot(taus, voltages, '.')
    plt.show()
    taus = np.asarray(taus)

    np.savetxt('data/sim/t2_taus.txt', taus)
    np.savetxt('data/sim/t2_voltages.txt', voltages)

    print(sim.runtime)
